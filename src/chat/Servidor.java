/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Servidor {
    int i = 0;
    ArrayList<Conexion> conexiones = null;
    
    public Servidor() {
        conexiones = new ArrayList<Conexion>();
    }
    
    
    public static void main(String args[]){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                   
                new Servidor().run();
            }
        });     
        
    }
    
    public void run(){
        ServerSocket server = null;
        System.out.println("Iniciando proceso servidor...");
        try {                           
            server = new ServerSocket(1001);
            while(true){
                Conexion c = new Conexion(server.accept(), i++, this);                
                conexiones.add(c);                        
                c.start();
            }
        } catch (Exception e){
              System.out.println("Error en conexion: "+e.getMessage());
        }     
    }
    
    void send(int id, String nombre, String mensaje){
        for(i=0; i<conexiones.size(); i++){
            Conexion c = conexiones.get(i);
            
            if (c.id != id){
                c.send(nombre,mensaje);
            }                          
         }     
    } 
    
    class Conexion extends Thread {
        Socket s;
        public int id;
        Servidor padre;
        boolean conectado = false;

        PrintWriter out = null;
        BufferedReader in = null;
        BufferedReader stdIn = null; 
        
        String nombre;

        public Conexion(Socket _s, int _id, Servidor _padre){
            this.s = _s;
            this.id = _id;
            this.padre = _padre;

            try {
                out = new PrintWriter(this.s.getOutputStream(), true);

                in = new BufferedReader(
                            new InputStreamReader(this.s.getInputStream()));
                
                this.conectado = true;

            } catch (IOException ioe){
                System.out.println(ioe.getMessage());
            }
        }
        
        public void run(){
            String sMensajeEntrada="";
        try {
            
            System.out.println("Se recibe conexion desde la direccion: "+s.getInetAddress());
                                    
            nombre = in.readLine();
            
            System.out.println("Conectado: "+nombre);
            
            while(true){
                sMensajeEntrada = in.readLine();
                
                if (sMensajeEntrada!=null){
                    
                    if (sMensajeEntrada.equals("Salir")){
                        in.close();
                        out.close();
                        s.close();
                        break;
                    }
                                        
                    System.out.println(nombre+" dice: "+sMensajeEntrada);                               
                    padre.send(id,nombre,sMensajeEntrada);                                   
                }
            }                 
        } catch (Exception e){
            System.out.println(nombre+" salio del chat");
        }
        }
        
        private void send(String nom, String msg) {
            out.println(nom+" : "+msg);
        }
        
    }
}
