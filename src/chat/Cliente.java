/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cliente extends Thread{
    Socket s;
        PrintWriter out;
        BufferedReader in;  
        boolean salir = false;
    
        public Cliente(){
            try{
                this.s = new Socket("127.0.0.1",1001);

                out =   new PrintWriter(s.getOutputStream(), true);
            
                in = new BufferedReader(
                        new InputStreamReader(s.getInputStream()));
            }catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            } 
            
        }
        
    public void run(){ 
        String sMensaje="";
        try {
            while(!this.salir){                
                sMensaje = in.readLine();
                
                if (sMensaje!=null){
                    ventanas.VentanaCliente.jTextArea1.setText(ventanas.VentanaCliente.jTextArea1.getText()+"\n"+" < "+sMensaje);
                }                
            }                        
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }                           
    }
        
    public void send(String mensaje) {
        out.println(mensaje);
    }

    public void cerrar() {
        try {
            s.close();
            in.close();
            out.close();
            this.salir = true;
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /*public static void main(String args[]){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                   
                new Cliente().run();
            }
        });
    }  
    
    void run(){
        String sMensajeSalida;
        BufferedReader in =
                   new BufferedReader(new InputStreamReader(System.in));
        
        Mensajes m = new Mensajes();
        m.start();
        
        while(true){                               
            try {                
              sMensajeSalida = in.readLine();
                
                if (sMensajeSalida!=null){
                    System.out.print("> ");
                    m.send(sMensajeSalida);
                    
                    if (sMensajeSalida.toLowerCase().startsWith("salir")){
                        m.cerrar();
                        break;
                    }                    
                }
                                
            } catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }*/
}
    